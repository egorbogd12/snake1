// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/InputComponent.h"



// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation((FRotator(-90, 0, 0)));
	CreateSnakeActor();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);

}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform(FVector(0, 0, 0)));
}

void APlayerPawnBase::HandlePlayerVerticalInput(const float Value)
{
	if (Value == 0)
	{
		return;
	}
	
	if (IsValid(SnakeActor))
	{
		SetMovementDirection(Value > 0 ? EMoveDirection::Up : EMoveDirection::Down);
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(const float Value)
{
	if (Value == 0)
	{
		return;
	}
	
	if (IsValid(SnakeActor))
	{
		SetMovementDirection(Value > 0 ? EMoveDirection::Left : EMoveDirection::Right);
	}
}

void APlayerPawnBase::SetMovementDirection(const EMoveDirection Direction)
{
	// GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Some debug message!"));    
	
	const EMoveDirection PrevDirection = SnakeActor->LastMovementDirection;
	switch (Direction)
	{
	case EMoveDirection::Up:    if (PrevDirection == EMoveDirection::Down)  return; break;
	case EMoveDirection::Down:  if (PrevDirection == EMoveDirection::Up)    return; break;
	case EMoveDirection::Left:  if (PrevDirection == EMoveDirection::Right) return; break;
	case EMoveDirection::Right: if (PrevDirection == EMoveDirection::Left)  return; break;
	}

	SnakeActor->ChangeSnakeDirection(Direction);
}

